//
//  Utilities.swift
//  Appxi
//
//  Created by Victor Alejandria on 1/31/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import Foundation
import UIKit

struct Utilities {
    
    static func secondsToHoursMinutesSeconds (seconds : Double) -> (hours: Int, minutes: Int, seconds: Int) {
        let (hr,  minf) = modf (seconds / 3600)
        let (min, secf) = modf (60 * minf)
        return (hr.toInt, min.toInt, (60 * secf).toInt)
    }
    
    static func errorMessage(title: String, localizeDescription: String, localizeFailureReason: String) -> NSError {
        let userInfo: [NSObject : AnyObject] =
            [
                NSLocalizedDescriptionKey as NSObject :  NSLocalizedString("Error", value: localizeDescription, comment: "") as AnyObject,
                NSLocalizedFailureReasonErrorKey as NSObject : NSLocalizedString("Error", value: localizeFailureReason, comment: "") as AnyObject
        ]
        let error = NSError(domain: "ShiploopHttpResponseErrorDomain", code: 401, userInfo: userInfo)
        
        return error
    }
    
    static func alertMessage(viewController: UIViewController, title: String, message: String) {
        let alertView = UIAlertController(title: title,
                                          message: message, preferredStyle:.alert)
        let okAction = UIAlertAction(title: "Continuar", style: .default, handler: nil)
        alertView.addAction(okAction)
        viewController.presentVC(alertView)
    }
    
}

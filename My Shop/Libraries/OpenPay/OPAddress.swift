//
//  OPAddress.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/15/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import ObjectMapper

class OPAddress: Mappable {

    var city: String?
    var countryCode: String?
    var postalCode: String?
    var line1: String?
    var line2: String?
    var line3: String?
    var state: String?

    init() {
    }
    
    public init(with dictionary: Dictionary<String, Any>) {
        postalCode = dictionary["postal_code"] != nil ? dictionary["postal_code"] as! String : ""
        line1 = dictionary["line1"] != nil ? dictionary["line1"] as! String : ""
        line2 = dictionary["line2"] != nil ? dictionary["line2"] as! String : ""
        line3 = dictionary["line3"] != nil ? dictionary["line3"] as! String : ""
        city = dictionary["city"] != nil ? dictionary["city"] as! String : ""
        state = dictionary["state"] != nil ? dictionary["state"] as! String : ""
        countryCode = dictionary["country_code"] != nil ? dictionary["country_code"] as! String : ""
    }

    required init?(map: Map) {
        //Required
    }
    
    func mapping(map: Map) {
        city <- map["city"]
        countryCode <- map["country_code"]
        postalCode <- map["postal_code"]
        line1 <- map["line1"]
        line2 <- map["line2"]
        line3 <- map["line3"]
        state <- map["state"]
    }
}

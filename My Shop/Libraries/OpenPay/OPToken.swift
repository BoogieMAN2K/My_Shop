//
//  OPToken.swift
//  My Shop
//
//  Created by Victor Alejandria on 2/15/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

public class OPToken {
    var id: String
    var card: OPCard
    
    public init() {
        id = ""
        card = OPCard()
    }
    
    public init(with dictionary: Dictionary<String, Any>) {
        id = dictionary["id"] != nil ? dictionary["id"] as! String : ""
        card = OPCard(with: dictionary["card"] as! Dictionary<String, Any>)
    }
    
}

//
//  Openpay.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/15/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//
import ReactiveKit
import ObjectMapper
import Alamofire

public class Openpay {
    
    private static let API_URL_SANDBOX = "https://sandbox-api.openpay.mx"
    private static let API_URL_PRODUCTION = "https://api.openpay.mx"
    private static let API_VERSION = "v1"
    private static let API_REVISION = "1.1"
    private static let OP_MODULE_TOKENS = "tokens"
    private static let OP_HTTP_METHOD_POST = "POST"
    private static let OP_HTTP_METHOD_GET = "GET"
    private static let OPENPAY_IOS_VERSION = "1.0.1"
    private static let DC_MERCHANT_ID = 203000
    private static let OpenpayDomain = "com.openpay.ios.lib"
    private static let OPErrorMessageKey = "com.openpay.ios.lib:ErrorMessageKey"
    private static let OPAPIError = 9999;
    
    private var merchantId: String!
    private var apiKey: String!
    private var sessionID: String!
    private var isProductionMode: Bool!
    private var connection: NSURLConnection!
    private var request: NSMutableURLRequest!
    private var queue: OperationQueue!
    private var isDebug: Bool!
    private var processCard: OPCard!
    private var scrollView: UIScrollView!
    private var inview: UIView!
    private var successCard: (() -> Void)!
    private var failureCard: ((_ error: NSError) -> Void)!
    private var holderValid: Bool = false
    private var baseURI: String!
    
    public var URI: String!
    
    public init(isDebug: Bool = true) {
        self.initialize(isDebug: isDebug )
    }
    
    private func initialize(isDebug: Bool ) {
        if isDebug {
            self.apiKey = Payment.kSandboxApiKey
            self.merchantId = Payment.kSandboxMerchantID
            KDataCollector.shared().debug = true
            baseURI = Openpay.API_URL_SANDBOX
            KDataCollector.shared().environment = KEnvironment.test
        } else {
            self.apiKey = Payment.kProductionApiKey
            self.merchantId = Payment.kProductionMerchantID
            KDataCollector.shared().debug = false
            baseURI = Openpay.API_URL_PRODUCTION
            KDataCollector.shared().environment = KEnvironment.production
        }
        self.isDebug = isDebug
        
        KDataCollector.shared().merchantID = Openpay.DC_MERCHANT_ID
        KDataCollector.shared().locationCollectorConfig = KLocationCollectorConfig.requestPermission
        
        self.URI = "\(baseURI!)/\(Openpay.API_VERSION)/\(merchantId!)"
        
        print("Created DeviceCollector")
    }
    
    func createTokenWithCard(address: OPAddress, successFunction: @escaping (_ responseParams: OPToken) -> Void, failureFunction: @escaping (_ error: NSError) -> Void ) {
        processCard.address = address
        if (processCard.valid) {
            sendFunction(method: Openpay.OP_MODULE_TOKENS, data: Mapper<OPAddress>().mapDictionary(JSONObject: address), httpMethod: Openpay.OP_HTTP_METHOD_POST, successFunction: successFunction, failureFunction: failureFunction)
        } else {
            let cardErrors: Dictionary<String, Any> = [
                "errors":processCard.errors
            ]
            let error: NSError = NSError(domain: Openpay.OpenpayDomain, code: 1001, userInfo: cardErrors)
            failureFunction(error)
        }
    }
    
    public func getTokenWithId(tokenId: String,
                               successFunction: @escaping (_ responseParams: OPToken) -> Void,
                               failureFunction: @escaping (_ error: NSError) -> Void ) {
        
        sendFunction(method: String(format: "%@/%@",Openpay.OP_MODULE_TOKENS,tokenId), data: nil, httpMethod: Openpay.OP_HTTP_METHOD_GET, successFunction: successFunction, failureFunction: failureFunction)
        
    }
    
    public func createDeviceSessionId(successFunction: @escaping (_ sessionId: String) -> Void,
                                      failureFunction: @escaping (_ error: NSError) -> Void) {
        sessionID = UUID().uuidString
        sessionID = sessionID.replacingOccurrences(of: "-", with: "")
        
        //Call device collector
        KDataCollector.shared().timeoutInMS = 30000
        KDataCollector.shared().collect(forSession: sessionID) { (sessionID, success, error) in
            if success {
                successFunction(sessionID)
                if ( self.isDebug! ) {
                    print( "SessionID: \(sessionID)" )
                }
                
            } else {
                if ((error) != nil ) {
                    print( error!.localizedDescription )
                }
                let userInfoDict: Dictionary<String, Any> = ["errors": NSLocalizedString("error.json", bundle: Bundle(for: Openpay.self), comment: "Error JSON")]
                let OPError = NSError(domain: Openpay.OpenpayDomain, code: Openpay.OPAPIError, userInfo: userInfoDict)
                failureFunction(OPError)
            }
        }
        
    }
    
    private func sendFunction( method: String,
                               data: Dictionary<String, Any>!,
                               httpMethod: String,
                               successFunction: @escaping (_ responseParams: OPToken) -> Void,
                               failureFunction: @escaping (_ error: NSError) -> Void) {
        
        var operationError: NSError!
        let urlPath: String = String(format: "%@/v1/%@/%@", Openpay.API_URL_SANDBOX, self.merchantId, method)
        let url: URL = URL(string: urlPath)!
        let request = NSMutableURLRequest()
        request.url = url
        request.httpMethod = httpMethod
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.setValue(String(format: "application/json;revision=%@",Openpay.API_VERSION), forHTTPHeaderField:"Accept")
        request.setValue(String(format: "OpenPay-iOS-SW/%@", Openpay.OPENPAY_IOS_VERSION), forHTTPHeaderField:"User-Agent")
        
        
        let authStr: String = String(format: "%@:%@", self.apiKey, "")
        let authData: Data =  authStr.data(using: String.Encoding.ascii)!
        let authValue: String = authData.base64EncodedString(options: Data.Base64EncodingOptions.endLineWithCarriageReturn )
        
        request.setValue( String(format: "Basic %@",authValue), forHTTPHeaderField: "Authorization")
        
        do {
            if (data != nil) {
                let payloadData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                request.httpBody = payloadData
            }
        } catch let error as NSError {
            print(error.localizedDescription)
            failureFunction(error)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, errorUrl in
            
            if (self.isDebug==true) {  print( String(format: "%@ %@", NSLocalizedString("debug.request", bundle: Bundle(for: Openpay.self), comment: "Debug Request"), "\(url)") ) }
            
            if errorUrl != nil {
                if let httpResponse = response as? HTTPURLResponse {
                    print("error \(httpResponse.statusCode)")
                }
                if (self.isDebug==true) { print( String(format: "%@ %@", NSLocalizedString("error.request", bundle: Bundle(for: Openpay.self), comment: "Error JSON"), "\(errorUrl!)") ) }
                operationError = errorUrl as NSError!
                failureFunction(operationError)
                return
            } else {
                
            }
            
            let jsonDictionary: Dictionary<String, Any> = self.dictionaryFromJSONData(data: data!, outError: &operationError)
            if jsonDictionary["id"] != nil {
                if (self.isDebug==true) {  print(NSLocalizedString("debug.responseok", bundle: Bundle(for: Openpay.self), comment: "Debug Response OK")) }
                successFunction( OPToken.init(with: jsonDictionary) )
            } else {
                let userInfoDict: Dictionary<String, Any> = ["errors": NSLocalizedString("error.json", bundle: Bundle(for: Openpay.self), comment: "Error JSON")] // "The response from Openpay failed to get parsed into valid JSON."]
                operationError = NSError(domain: Openpay.OpenpayDomain, code: Openpay.OPAPIError, userInfo: userInfoDict)
                print("Parsing Response Error: \(operationError.code): \(operationError.localizedDescription)")
                failureFunction(operationError!)
            }
        }
        
        task.resume()
    }
    
    private func dictionaryFromJSONData(data: Data, outError: inout NSError!) -> Dictionary<String, Any>! {
        var jsonDictionary: Dictionary<String, Any>!
        do {
            jsonDictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            if (jsonDictionary == nil) {
                if (outError==nil) {
                    let userInfoDict: Dictionary<String, Any> = ["errors": NSLocalizedString("error.json", bundle: Bundle(for: Openpay.self), comment: "Error JSON")]
                    outError = NSError(domain: Openpay.OpenpayDomain, code: Openpay.OPAPIError, userInfo: userInfoDict)
                    print("dictionaryFromJSONData Error: \(outError)")
                }
                return nil;
            }
        } catch {
            print("dictionaryFromJSONData Error: \(outError)")
        }
        
        return jsonDictionary;
    }
    
    private func parseInt(string: String!) -> Int {
        var value: Int = 0
        if let intVal = Int(string) {
            value = intVal
        }
        return value
    }
    
    private func formatCardNumber(cardNumber: String, type: OPCard.OPCardType) -> String {
        let cleanNumber: String = cardNumber.replacingOccurrences(of: " ", with: "")
        let separator: Character = " "
        var segments: [Int] = [0]
        var outNumber: String = ""
        
        switch type {
        case OPCard.OPCardType.OPCardTypeVisa:
            segments = [4,8,12,16]
        case OPCard.OPCardType.OPCardTypeMastercard:
            segments = [4,8,12,16]
        case OPCard.OPCardType.OPCardTypeAmericanExpress:
            segments = [4,10,15]
        case OPCard.OPCardType.OPCardTypeUnknown:
            segments = [4,8,12,16]
        }
        
        var ci: Int = 0;
        var cp: Int = 0;
        for i in cleanNumber.characters {
            if( segments[0] != 0 && cp == segments[ci] ) {
                outNumber.characters.append(separator)
                if(segments.count > 1) {
                    if(ci < segments.count) {
                        ci += 1
                    }else{
                        ci = 0
                    }
                }
            }
            outNumber.characters.append(i)
            cp += 1
        }
        
        return outNumber
    }
    
    static func get<T: Mappable, E: OPError>(url: URLConvertible, parameters: Parameters?,
                    encoding: ParameterEncoding = JSONEncoding.default, resultType: T.Type,
                    completionHandler: @escaping (DataResponse<T>, AtomicObserver<T, E>) -> Void ) -> Signal<T, E> {
        let response = Signal <T, E> { observer in
            let openpay = Openpay()
            
            let authStr = "\(openpay.apiKey!):"
            let authData: Data =  authStr.data(using: String.Encoding.ascii)!
            let authValue: String = authData.base64EncodedString(options: Data.Base64EncodingOptions.endLineWithCarriageReturn )
            let headers: HTTPHeaders = [
                "Authorization": "Basic \(authValue)",
                "Accept": "application/json;revision=\(Openpay.API_REVISION)",
                "User-Agent": "OpenPay-iOS-SW/\(Openpay.OPENPAY_IOS_VERSION)"
            ]
            
            _ = Alamofire.request("\(openpay.URI!)/\(url)", method: .get, parameters: parameters, encoding: encoding, headers: headers).responseObject(completionHandler: {
                (response: DataResponse<T>) in completionHandler(response, observer)
            })
            
            return NonDisposable.instance
        }
        
        return response
    }
    
	static func getArray<T: Mappable, E: OPError>(url: URLConvertible, parameters: Parameters?,
	                     encoding: ParameterEncoding = JSONEncoding.default,
	                     resultType: T.Type,
	                     completionHandler: @escaping (DataResponse<[T]>, AtomicObserver<[T], E>) -> Void ) -> Signal<[T], E> {
		let response = Signal <[T], E> { observer in
			let openpay = Openpay()

			let authStr = "\(openpay.apiKey!):"
			let authData: Data =  authStr.data(using: String.Encoding.ascii)!
			let authValue: String = authData.base64EncodedString(options: Data.Base64EncodingOptions.endLineWithCarriageReturn )
			let headers: HTTPHeaders = [
				"Authorization": "Basic \(authValue)",
				"Accept": "application/json;revision=\(Openpay.API_REVISION)",
				"User-Agent": "OpenPay-iOS-SW/\(Openpay.OPENPAY_IOS_VERSION)"
			]

			_ = Alamofire.request("\(openpay.URI!)/\(url)", method: .get, parameters: parameters, encoding: encoding, headers: headers).responseArray(completionHandler: {
				(response: DataResponse<[T]>) in completionHandler(response, observer)
			})

			return NonDisposable.instance
		}

		return response
	}

	static func post<T: Mappable, E: OPError>(url: String, parameters: Parameters,
	                 encoding: ParameterEncoding = JSONEncoding.default, resultType: T.Type,
	                 completionHandler: @escaping (DataResponse<T>, AtomicObserver<T, E>) -> Void ) -> Signal<T, E> {
        let response = Signal <T, E> { observer in
            let openpay = Openpay()
            
            let authStr = "\(openpay.apiKey!):"
            let authData: Data =  authStr.data(using: String.Encoding.ascii)!
            let authValue: String = authData.base64EncodedString(options: Data.Base64EncodingOptions.endLineWithCarriageReturn )
            let headers: HTTPHeaders = [
                "Authorization": "Basic \(authValue)",
                "Accept": "application/json;revision=\(Openpay.API_REVISION)",
                "User-Agent": "OpenPay-iOS-SW/\(Openpay.OPENPAY_IOS_VERSION)"
            ]
            
            _ = Alamofire.request("\(openpay.URI!)/\(url)", method: .post, parameters: parameters, encoding: encoding, headers: headers).responseObject(completionHandler: {
                (response: DataResponse<T>) in completionHandler(response, observer)
            })
            
            return NonDisposable.instance
        }
        
        return response
    }
    
}



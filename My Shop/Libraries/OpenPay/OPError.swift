//
//  OpenpayError.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/15/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import ObjectMapper

class OPError: Mappable, Error {
    
    var category: String?
    var errorCode: Int?
    var errorDescription: String?
    var httpCode: Int?
    var requestId: String?
    var fraudRules: Array<String>?
    
    init() {
    }
    
    required init?(map: Map) {
        //Required
    }
    
    func mapping(map: Map) {
        category <- map["category"]
        errorCode <- map["error_code"]
        errorDescription <- map["description"]
        httpCode <- map["http_code"]
        requestId <- map["request_id"]
        fraudRules <- map["fraud_rules"]
    }
    
}

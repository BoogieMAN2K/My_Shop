//
//  OPStore.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/15/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import ObjectMapper

class OPStore: Mappable {

    var reference: String?
    var barcodeURL: String?
    var paybinReference: String?
    var barcodePaybinURL: String?
    
    init() {
    }
    
    required init?(map: Map) {
        //Required
    }
    
    func mapping(map: Map) {
        reference <- map["reference"]
        barcodeURL <- map["barcode_url"]
        paybinReference <- map["paybin_reference"]
        barcodePaybinURL <- map["barcode_paybin_url"]
    }

}

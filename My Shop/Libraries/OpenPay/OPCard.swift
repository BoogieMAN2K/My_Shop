//
//  OPCard.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/16/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import ReactiveKit
import ObjectMapper
import Alamofire
import SwiftyJSON

class OPCard: Mappable {
    
    var identifier: String!
    var creationDate: Date!
    var holderName: String!
    var cardNumber: String! {
        didSet {
            if (cardNumber != nil) {
                cardNumber = cardNumber.replacingOccurrences(of: " ", with: "")
            }
        }
    }
    var cvv2: String!
    var expirationMonth: Int!
    var expirationYear: Int!
    var address: OPAddress!
    var allowsChange: Bool!
    var allowsPayout: Bool!
    var brand: String!
    var type: String!
    var bankName: String!
    var bankCode: String!
    var customerId: String!
    var pointsCard: Bool!
    public var errors: NSMutableArray!
    public var expired: Bool {
        let cleanNumber1 = expirationMonth.toString.replacingOccurrences(of: " ", with: "")
        let cleanNumber2 = expirationYear.toString.replacingOccurrences(of: " ", with: "")
        if ( cleanNumber1 == "" || cleanNumber2 == "" ) { return true; }
        
        if ( self.expirationMonth > 12 || self.expirationMonth < 1 ) { return true; }
        
        let date = Date()
        let calendar = Calendar.current
        let currentMonth: Int = calendar.component(.month, from: date)
        let currentYear: Int = calendar.component(.year, from: date)
        
        var year: Int = self.expirationYear!
        if ( (expirationYear.toString.characters.count) <= 2) {
            year += 2000;
        }
        
        if (year < currentYear) { return true; }
        
        return (year == currentYear && self.expirationMonth! < currentMonth);
    }
    public var cardType: OPCardType {
        let cleanNumber = cardNumber.replacingOccurrences(of: " ", with: "")
        if ( cleanNumber == "" || (cleanNumber.characters.count < 2)) {
            return OPCardType.OPCardTypeUnknown;
        }
        
        let digits: Int = Int(cardNumber.substring(to: cardNumber.index(cardNumber.startIndex, offsetBy: 2)))!
        
        if (digits >= 40 && digits <= 49) {
            return OPCardType.OPCardTypeVisa
        } else if (digits >= 50 && digits <= 59) {
            return OPCardType.OPCardTypeMastercard;
        } else if (digits == 34 || digits == 37) {
            return OPCardType.OPCardTypeAmericanExpress;
        } else {
            return OPCardType.OPCardTypeUnknown;
        }
    }
    public var numberValid: Bool {
        let cleanNumber = cardNumber.replacingOccurrences(of: " ", with: "")
        if ( cleanNumber == "" ) { return false; }
        if ( self.cardNumber.characters.count < 12 ) { return false; }
        
        var odd: Bool = true;
        var total: Int = 0;
        
        for i in stride(from: cardNumber.characters.count-1, to: -1, by: -1) {
            let start = cardNumber.index(cardNumber.startIndex, offsetBy: i)
            let end = cardNumber.index(cardNumber.startIndex, offsetBy: i+1)
            let range = start..<end
            let value: Int = Int( cardNumber.substring(with: range) )!
            odd = !odd
            total += odd ? 2 * value - (value > 4 ? 9 : 0) : value;
        }
        
        return (total % 10) == 0;
    }
    public var valid: Bool {
        var valid: Bool = true;
        
        if (!self.numberValid) {
            errors = NSMutableArray.init()
            errors?.add("Card number is not valid")
            valid = false;
        }
        
        if (self.expired) {
            errors = NSMutableArray.init()
            errors?.add("Card expired is not valid")
            valid = false;
        }
        
        return valid;
    }
    public var securityCodeCheck: OPCardSecurityCodeCheck {
        if (self.cardType == OPCardType.OPCardTypeUnknown) { return OPCardSecurityCodeCheck.OPCardSecurityCodeCheckUnknown; }
        let cleanCvv2 = cvv2?.replacingOccurrences(of: " ", with: "")
        if (cleanCvv2 == "") {
            return OPCardSecurityCodeCheck.OPCardSecurityCodeCheckUnknown;
        }else{
            let requiredLength = (self.cardType==OPCardType.OPCardTypeAmericanExpress) ? 4 : 3;
            if ( self.cvv2.characters.count == requiredLength) {
                return OPCardSecurityCodeCheck.OPCardSecurityCodeCheckPassed;
            }
            else {
                return OPCardSecurityCodeCheck.OPCardSecurityCodeCheckFailed;
            }
        }
    }
    
    public enum OPCardType: Int {
        case
        OPCardTypeUnknown,
        OPCardTypeVisa,
        OPCardTypeMastercard,
        OPCardTypeAmericanExpress
    }
    
    public enum OPCardSecurityCodeCheck: Int {
        case
        OPCardSecurityCodeCheckUnknown,
        OPCardSecurityCodeCheckPassed,
        OPCardSecurityCodeCheckFailed
    }
    
    init() {
    }
    
    public init(with dictionary: Dictionary<String, Any>) {
        creationDate = NSDate() as Date!
        identifier = ""
        bankName = ""
        allowsPayout = false
        brand = ""
        allowsChange = false
        bankCode = ""
        errors = NSMutableArray()
        
        holderName = dictionary["holder_name"] != nil ? dictionary["holder_name"] as! String : ""
        expirationYear = dictionary["expiration_year"] != nil ? dictionary["expiration_year"] as! Int : 0
        expirationMonth = dictionary["expiration_month"] != nil ? dictionary["expiration_month"] as! Int : 0
        cvv2 = dictionary["cvv2"] != nil ? dictionary["cvv2"] as! String : ""
        self.cardNumber =  dictionary["card_number"] != nil ? dictionary["card_number"] as! String : "0000000000000000"
        
        if let addressDict = dictionary["address"] as? Dictionary<String, Any> {
            address = OPAddress( with: addressDict )
        }
    }
    
    required init?(map: Map) {
        //Required
    }
    
    func mapping(map: Map) {
        identifier <- map["id"]
        holderName <- map["holder_name"]
        creationDate <- map["creation_date"]
        cardNumber <- map["card_number"]
        cvv2 <- map["cvv2"]
        expirationMonth <- map["expiration_month"]
        expirationYear <- map["expiration_year"]
        address <- map["address"]
        allowsChange <- map["allows_change"]
        allowsPayout <- map["allows_payout"]
        brand <- map["brand"]
        type <- map["type"]
        bankName <- map["bank_name"]
        bankCode <- map["bank_code"]
        customerId <- map["customer_id"]
        pointsCard <- map["points_card"]
    }
    
    static func create(card: OPCard, client: String) -> Signal<OPCard, OPError> {
        let parameters = card.toJSON()
        let result = Openpay.post(url: "customers/\(client)/cards", parameters: parameters, encoding: JSONEncoding.default, resultType: OPCard.self) { (response, observer) in
            if response.result.isSuccess {
                if (response.result.value?.identifier != nil) {
					//TODO: Agregar actualización del id de la tarjeta usada para cancelar las operaciones (traido de Openpay)
                    observer.next(response.result.value!)
                } else {
                    let json = JSON(data: response.data!)
                    let error = OPError()
                    error.category = json["category"].stringValue
                    error.errorCode = json["error_code"].intValue
                    error.errorDescription = json["error_description"].stringValue
                    error.httpCode = json["http_code"].intValue
                    observer.failed(error)
                }
            } else {
                observer.failed(response.result.error as! OPError)
            }
            
            observer.completed()
        }
        
        return result
    }
    
    static func retrieve(identifier: String, client: String) -> Signal<OPCard, OPError> {
        let result = Openpay.get(url: "customers/\(client)/cards/\(identifier)", parameters: nil, resultType: OPCard.self) { (response, observer) in
            if response.result.isSuccess {
                if (response.result.value?.identifier != nil) {
                    observer.next(response.result.value!)
                } else {
                    let json = JSON(data: response.data!)
                    let error = OPError()
                    error.category = json["category"].stringValue
                    error.errorCode = json["error_code"].intValue
                    error.errorDescription = json["error_description"].stringValue
                    error.httpCode = json["http_code"].intValue
                    observer.failed(error)
                }
            } else {
                observer.failed(response.result.error as! OPError)
            }
            
            observer.completed()
        }
        
        return result
    }

	static func retrieveAll(client: String) -> Signal<[OPCard], OPError> {
		let result = Openpay.getArray(url: "customers/\(client)/cards/", parameters: nil,
		                              resultType: OPCard.self) { (response, observer) in
										if response.result.isSuccess {
											if (!(response.result.value?.isEmpty)!) {
												observer.next(response.result.value!)
											} else {
												let json = JSON(data: response.data!)
												let error = OPError()
												error.category = json["category"].stringValue
												error.errorCode = json["error_code"].intValue
												error.errorDescription = json["error_description"].stringValue
												error.httpCode = json["http_code"].intValue
												observer.failed(error)
											}
										} else {
											observer.failed(response.result.error as! OPError)
										}
										
										observer.completed()
		}
		
		return result
	}

}

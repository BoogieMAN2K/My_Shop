//
//  OPClient.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/15/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import ReactiveKit
import ObjectMapper
import Alamofire
import SwiftyJSON

class OPClient: Mappable {
    
    var identifier: String?
    var externalID: String?
    var creationDate: Date?
    var name: String?
    var lastname: String?
    var email: String?
    var phone: String?
    var status: String?
    var balance: Double?
    var clabe: String?
    var address: OPAddress?
    var store: OPStore?
    var requiresAccount: Bool?
    
    init() {
    }
    
    required init?(map: Map) {
        //Required
    }
    
    func mapping(map: Map) {
        identifier <- map["id"]
        externalID <- map["external_id"]
        creationDate <- map["creation_date"]
        name <- map["name"]
        lastname <- map["last_name"]
        email <- map["email"]
        phone <- map["phone_number"]
        status <- map["status"]
        balance <- map["balance"]
        clabe <- map["clabe"]
        address <- map["address"]
        store <- map["store"]
        requiresAccount <- map["requires_account"]
    }
	
    static func create (client: OPClient) -> Signal<OPClient, OPError> {
        let parameters = client.toJSON()
        let result = Openpay.post(url: "customers", parameters: parameters, encoding: JSONEncoding.default,
                                  resultType: OPClient.self) { (response, observer) in
            if response.result.isSuccess {
                if (response.result.value?.identifier != nil) {
					//TODO: Agregar actualización del usuario agregando el id de OpenPay.
                    observer.next(response.result.value!)
                } else {
                    let json = JSON(data: response.data!)
                    let error = OPError()
                    error.category = json["category"].stringValue
                    error.errorCode = json["error_code"].intValue
                    error.errorDescription = json["error_description"].stringValue
                    error.httpCode = json["http_code"].intValue
                    observer.failed(error)
                }
            } else {
                observer.failed(response.result.error as! OPError)
            }
            
            observer.completed()
        }
        
        return result
    }
    
    static func get(client: String) -> Signal<OPClient, OPError> {
        let result = Openpay.get(url: "customers/\(client)", parameters: nil, encoding: JSONEncoding.default,
                                 resultType: OPClient.self) { (response, observer) in
            if response.result.isSuccess {
                if (response.result.value?.identifier != nil) {
					//TODO: Agregar actualización del usuario agregando el id de OpenPay.
                } else {
                    let json = JSON(data: response.data!)
                    let error = OPError()
                    error.category = json["category"].stringValue
                    error.errorCode = json["error_code"].intValue
                    error.errorDescription = json["error_description"].stringValue
                    error.httpCode = json["http_code"].intValue
                    observer.failed(error)
                }
            } else {
                observer.failed(response.result.error as! OPError)
            }
            
            observer.completed()
        }
        
        return result
    }
    
}

//
//  Protocols.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import Foundation

protocol ShoppingCartDelegate {
    
    typealias productOrderType = (product:Product, quantity:Int)

    func addToShoppingCart(productOrder: ProductOrder)
    
}

protocol ProcessPaymentDelegate {
	func processPayment(with CreditCard: String)
}

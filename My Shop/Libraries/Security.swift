//
//  Security.swift
//  My Shop
//
//  Created by Victor Alejandria on 26/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import EZSwiftExtensions
import LocalAuthentication
import ReactiveKit
import IWCocoa

struct Security {
    static func touchIDLogIn(viewController: UIViewController, withCompletionHandler completionHandler: @escaping () -> Void) {
        if LAContext().canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error:nil) {
            LAContext().evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics,
                                       localizedReason: "Iniciando sesión con Touch ID",
                                       reply: { (success : Bool, error : NSError? ) -> Void in
                                        DispatchQueue.main.async(execute: {
                                            if success {
                                                completionHandler()
                                            }
                                            
                                            if error != nil {
                                                var message : NSString
                                                var showAlert : Bool
                                                switch(error!.code) {
                                                case LAError.authenticationFailed.rawValue:
                                                    message = "Ha habido un problema verificando su identidad"
                                                    showAlert = true
                                                    break;
                                                case LAError.userCancel.rawValue:
                                                    message = "Se cancelo el inicio de sesión."
                                                    showAlert = true
                                                    break;
                                                case LAError.userFallback.rawValue:
                                                    message = "You pressed password."
                                                    showAlert = true
                                                    break;
                                                default:
                                                    showAlert = true
                                                    message = "Touch ID puede no estar configurado"
                                                    break;
                                                }
                                                
                                                let alertView = UIAlertController(title: "Error",
                                                                                  message: message as String, preferredStyle:.alert)
                                                let okAction = UIAlertAction(title: "Continuar", style: .default, handler: nil)
                                                alertView.addAction(okAction)
                                                if showAlert {
                                                    viewController.presentVC(alertView)
                                                }
                                            }
                                        })
                                        
                                        } as! (Bool, Error?) -> Void)
        } else {
            completionHandler()
        }
    }
    
    static func checkLogin(username: String, password: String ) -> Signal<Bool, NSError> {
        let parameters = ["_username" : username, "_password" : password] as [String:Any]        
        let userSignal = Services.request(url: "login_check", parameters: parameters, method: .post, headers: ["Content-Type":"application/json"], returnType: Token())
        
        return userSignal.map {
            if $0.1.token == nil {
                UserDefaults.standard.set(false, forKey: "hasLoginKey")
                return false
            }
            
            UserDefaults.standard.setValue($0.1.token!, forKey: "token")
            return $0.0
        }
    }
    
}

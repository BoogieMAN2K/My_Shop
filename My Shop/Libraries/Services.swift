//
//  Services.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/20/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import ObjectMapper
import Alamofire
import AlamofireObjectMapper
import AlamofireImage
import ReactiveKit
import IWCocoa

struct Services {
    
    public let baseURL: String
    
    static func request<T: Mappable>(url: String, parameters: Parameters? = nil, method: HTTPMethod, encoding: ParameterEncoding = JSONEncoding.default, headers: HTTPHeaders = ["Authorization": "Bearer \(User().userToken!)" ], returnType: T) -> Signal<(Bool, T), NSError> {
        let responseObject = Signal<(Bool, T), NSError> { observer in
            _ = Alamofire.request("\(WebService.kServerBaseURL)\(url)", method: method, parameters: parameters, encoding: encoding, headers: headers).responseObject(completionHandler: { (response: DataResponse<T>) in
                let json = JSON(data: response.data!)["user"]
                let message = json["message"].string
                if response.result.isSuccess {
                    if (message != String.init()) {
                        observer.next((true, response.result.value!))
                    } else {
                        //FIXME: Buscar una forma de devolver un elemento vacio de tipo Mappable en caso de error.
                        observer.next((false, response.result.value!))
                    }
                } else {
                    observer.failed(response.result.error! as NSError)
                }
                
                observer.completed()
            })
            
            return NonDisposable.instance
        }
        
        return responseObject
    }
    
    static func requestArray<T: Mappable>(url: String, parameters: [String: Any]? = nil, method: HTTPMethod, encoding: ParameterEncoding = JSONEncoding.default, headers: HTTPHeaders = ["Authorization": "Bearer \(User().userToken!)" ], returnType: [T]) -> Signal<(Bool, [T]), NSError> {
        let responseObject = Signal<(Bool, [T]), NSError> { observer in
            _ = Alamofire.request("\(WebService.kServerBaseURL)\(url)", method: method, parameters: parameters, encoding: encoding, headers: headers).responseArray(completionHandler: { (response: DataResponse<[T]>) in
                let json = JSON(data: response.data!)
                let message = json["message"].string
                if response.result.isSuccess {
                    if (message != String.init()) {
                        observer.next((true, response.result.value!))
                    } else {
                        observer.next((false, []))
                    }
                } else {
                    observer.failed(response.result.error! as NSError)
                }
                
                observer.completed()
            })
            
            return NonDisposable.instance
        }
        
        return responseObject
    }
    
}

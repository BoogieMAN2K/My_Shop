//
//  ProductDetailViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 30/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import KRProgressHUD
import ObjectMapper

class ProductDetailViewController: UIViewController, ShoppingCartDelegate {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var productDescription: UITextView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var shopProduct: IconButton!
    
    var productId: Int!
    var selectedProduct: Product!

    var shoppingCart = [ProductOrder]()
    
    @IBAction func shop(_ sender: IconButton) {
        self.performSegue(withIdentifier: "ProductQuantitySegue", sender: self)
    }
    
    func addToShoppingCart(productOrder: ProductOrder) {
        self.shoppingCart.append(productOrder)
        self.performSegue(withIdentifier: "ShopCartSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        KRProgressHUD.show()
        let productSignal = Product.show(id: self.productId!)
        _ = productSignal.observeNext { (product) in
            KRProgressHUD.dismiss()
            self.selectedProduct = product
            self.productImage.af_setImage(withURL: product.image)
            self.name.text = product.name!
            self.productDescription.text = product.description
            self.price.text = "$\(product.price.toString)"
        }

		guard let shoppingString = UserDefaults.standard.string(forKey: "shoppingCart") else { return }
		shoppingCart = Mapper<ProductOrder>().mapArray(JSONString: shoppingString)!
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: ShopProductViewController.self) {
            let destinationVC = segue.destination as! ShopProductViewController
            destinationVC.delegate = self
            destinationVC.selectedProduct = self.selectedProduct
        }
        
        if segue.destination.isKind(of: ShoppingCartViewController.self) {
            let destinationVC = segue.destination as! ShoppingCartViewController
            destinationVC.shoppingCart = self.shoppingCart
        }
    }

}

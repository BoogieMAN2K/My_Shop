//
//  ProductListTableViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 30/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import KRProgressHUD

class ProductListTableViewController: UITableViewController {

    var productList: [Product]!
    let reuseIdentifier = "productCell"
    var categoryId: Int!
    var selectedProduct: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.productList = []
        
        self.tableView.isScrollEnabled = true
        self.tableView.register(UINib(nibName: "ProductsTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        
        KRProgressHUD.show()
        let productsSignal = Product.forCategory(id: self.categoryId!)
        _ = productsSignal.observeNext {
            KRProgressHUD.dismiss()
            self.productList = $0
            self.tableView.reloadData()
        }

        self.clearsSelectionOnViewWillAppear = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.toCGFloat
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedProduct = productList[indexPath.row].id
        self.performSegue(withIdentifier: "ProductSegue", sender: self)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ProductsTableViewCell
        let product = productList[indexPath.row]
        
        cell.productImage.af_setImage(withURL: product.image!)
        cell.name.text = product.name
        cell.smallDescription.text = product.description
        cell.price.text = "$\(product.price.toString)"

        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: ProductDetailViewController.self) {
            let destinationVC = segue.destination as! ProductDetailViewController
            destinationVC.productId = self.selectedProduct
        }
    }

}

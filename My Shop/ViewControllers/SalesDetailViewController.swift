//
//  SalesDetailViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 2/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit

class SalesDetailViewController: UIViewController {

    var selectedOffer: Offer!
    
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.storeImage.af_setImage(withURL: selectedOffer.image ?? URL.init(string: "")!)
        self.name.text = self.selectedOffer.description ?? ""
        let startDate = self.selectedOffer.start ?? Date.init()
        self.startDate.text = "Fecha de Inicio: \(startDate.toString(format: "dd/MM/yyyy"))"
        let endDate = self.selectedOffer.finish ?? Date.init()
        self.endDate.text = "Fecha de Fin: \(endDate.toString(format: "dd/MM/yyyy"))"
        
    }

}

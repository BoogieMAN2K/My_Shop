//
//  RecoverUserViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 30/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import KRProgressHUD

class RecoverUserViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var email: IconTextField!
    @IBOutlet weak var recover: IconButton!
    @IBOutlet weak var createUser: IconButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var activeField: UITextField!
    
    @IBAction func recoverPassword(_ sender: IconButton) {
		KRProgressHUD.show()
        _ = User.recoverPassword(email: email.text!).observeNext(with: { (result) in
            KRProgressHUD.dismiss()
            if result {
				self.popToRootVC()
                Utilities.alertMessage(viewController: self, title: "Solicitud enviada", message: "Se le ha enviado un correo con instrucciones para recuperar su contraseña")
            } else {
                Utilities.alertMessage(viewController: self, title: "Error", message: "Ha ocurrido un error solicitando la recuperación de contraseña, por favor intente mas tarde")
              _ = Utilities.errorMessage(title: "Error solicitando recuperación", localizeDescription: "Ha ocurrido un error solicitando la recuperación de contraseña, por favor intente mas tarde", localizeFailureReason: "Posible problema de comunicación con el servidor")
            }
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.email.delegate = self
        
        self.registerForKeyboardNotifications()
        self.hideKeyboardWhenTappedAround()
    }

    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func unRegisterForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWasShown(_ aNotification: NSNotification) {
        var info = aNotification.userInfo!
        let kbSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        var aRect = self.view.frame
        aRect.size.height -= kbSize.height
        guard let finalActiveField = self.activeField else { return }
        if !aRect.contains(finalActiveField.frame.origin) {
            self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
        }
    }
    
    func keyboardWillBeHidden(_ aNotification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeField = nil
    }
}

//
//  StoreDetailViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class StoreDetailViewController: UIViewController {

    var selectedStore: Store!
    
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phone1: UILabel!
    @IBOutlet weak var phone2: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.storeImage.af_setImage(withURL: selectedStore.image ?? URL.init(string: "")!)
        self.name.text = self.selectedStore.name ?? ""
        self.address.text = "Dirección: \(self.selectedStore.address ?? "")"
        self.phone1.text = "Telefono principal: \(self.selectedStore.mainPhone ?? "")"
        self.phone1.text = "Teléfono adicional: \(self.selectedStore.secondPhone ?? "")"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

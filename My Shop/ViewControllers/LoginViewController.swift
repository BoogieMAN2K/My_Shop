//
//  LoginViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 27/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import EZSwiftExtensions
import KRProgressHUD
import LocalAuthentication

class LoginViewController: BaseViewController {

	@IBOutlet weak var login: IconTextField!
	@IBOutlet weak var password: IconTextField!
	@IBOutlet weak var recoverPassword: UIButton!
	@IBOutlet weak var register: UIButton!
	@IBOutlet weak var scrollView: UIScrollView!

	let MyKeychainWrapper = KeychainWrapper()
	var context = LAContext()

	@IBAction func loginAction(_ sender: IconButton) {
		if (login.validateEmpty().isValid || password.validateEmpty().isValid) {
			Utilities.alertMessage(viewController: self, title: "Error iniciando sesión", message: "Usuario o contraseña incorrecta, intente nuevamente")
			return;
		}

		login.resignFirstResponder()
		password.resignFirstResponder()

		let hasLoginKey = UserDefaults.standard.bool(forKey: "hasLoginKey")
		if hasLoginKey == false {
			UserDefaults.standard.setValue(self.login.text, forKey: "username")
		}
		MyKeychainWrapper.setObject(password.text, forKey:kSecValueData)
		MyKeychainWrapper.writeToKeychain()
		UserDefaults.standard.set(true, forKey: "hasLoginKey")
		UserDefaults.standard.synchronize()

		KRProgressHUD.show()
		_ = User.login(username: self.login.text!, password: self.password.text!).observeNext { result in
			if result {
				let userSignal = User.info()
				_ = userSignal.observeNext(with: { _ in
					KRProgressHUD.dismiss()
					self.view.window?.rootViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
				})
			} else {
				KRProgressHUD.dismiss()
				Utilities.alertMessage(viewController: self, title: "Error iniciando sesión", message: "Usuario o contraseña incorrecta, intente nuevamente")
			}
		}
	}

	@IBAction func registerAction(_ sender: UIButton) {
		self.performSegue(withIdentifier: "RegisterSegue", sender: self)
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		self.login.becomeFirstResponder()

		let textFields: [UITextField] = [self.login, self.password]
		self.registerForAutoScroll(scrollView: scrollView, textFields: textFields)

	}

	override func viewWillAppear(_ animated: Bool) {
		self.navigationController?.navigationBar.isHidden = true
	}

	override func viewWillDisappear(_ animated: Bool) {
		self.navigationController?.navigationBar.isHidden = false
	}

	@IBAction func recoverPassword(_ sender: UIButton) {
		self.performSegue(withIdentifier: "RecoverSegue", sender: self)
	}

}

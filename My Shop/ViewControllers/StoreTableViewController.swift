//
//  StoreTableViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import KRProgressHUD

class StoreTableViewController: UITableViewController {

    var stores = [Store]()
    let reuseIdentifier = "productCell"
    var selectedStore: Store!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: "ProductsTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)

        let storeSignal = Store.all()
        KRProgressHUD.show()
        _ = storeSignal.observeNext(with: { (result) in
            KRProgressHUD.dismiss()
            self.stores = result
            self.tableView.reloadData()
        })
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stores.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ProductsTableViewCell
        let store = stores[indexPath.row]
        
        cell.productImage.af_setImage(withURL: store.image!)
        cell.name.text = store.name
        cell.smallDescription.text = store.address
        cell.price.isHidden = true
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedStore = stores[indexPath.row]
        
        self.performSegue(withIdentifier: "StoreDetail", sender: self)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.toCGFloat
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: StoreDetailViewController.self) {
            let destinationVC = segue.destination as! StoreDetailViewController
            destinationVC.selectedStore = self.selectedStore
        }
    }
}

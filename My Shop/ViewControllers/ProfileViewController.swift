//
//  ProfileViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import KRProgressHUD

class ProfileViewController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var profilePicture: ProfilePictures!
    @IBOutlet weak var name: IconTextField!
    @IBOutlet weak var phone: IconTextField!
    @IBOutlet weak var email: IconTextField!
    @IBOutlet weak var address: IconTextField!
    @IBOutlet weak var identityDocument: IconTextField!
    @IBOutlet weak var confirm: IconButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var selectPicture: UITapGestureRecognizer!
    
    var activeField: UITextField!
    
    @IBAction func confirmChanges(_ sender: IconButton) {
        let updatePictureSignal = User.updateProfilePicture(picture: profilePicture.image!)
        
        KRProgressHUD.show()
        _ = updatePictureSignal.observeNext(with: { (result) in
            if result {
                KRProgressHUD.dismiss()
                Utilities.alertMessage(viewController: self, title: "Imagen Guardada", message: "")
            }
        })
        
        var newUser = User()
        newUser.name = self.name.text!
        newUser.phone = self.phone.text!
        newUser.email = self.email.text!
        let updateProfileSignal = User.update(user: newUser)
        KRProgressHUD.show()
        _ = updateProfileSignal.observeNext(with: { (result) in
            KRProgressHUD.dismiss()
            if (result) {
                Utilities.alertMessage(viewController: self, title: "Datos Guardados", message: "Se han actualizado los datos del perfil correctamente")
            } else {
                Utilities.alertMessage(viewController: self, title: "Error", message: "Ha ocurrido un error mientras se guardaba la información, por favor intente nuevamente")
            }
        })
    }
    
    @IBAction func selectPicture(_ sender: UITapGestureRecognizer) {
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.allowsEditing = false
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profilePicture.contentMode = .scaleAspectFill
            profilePicture.image = pickedImage
        }
        
        dismiss(animated: true, completion: { self.name.becomeFirstResponder() })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.name.delegate = self
        self.phone.delegate = self
        self.email.delegate = self
        
        self.registerForKeyboardNotifications()
        self.hideKeyboardWhenTappedAround()
        
        self.name.becomeFirstResponder()
        self.profilePicture.image = #imageLiteral(resourceName: "FotoPerfil")
        
        let userSignal = User.info()
        KRProgressHUD.show()
        
        _ = userSignal.observeNext(with: { (user) in
            KRProgressHUD.dismiss()
            self.profilePicture.af_setImage(withURL: user.image, placeholderImage: #imageLiteral(resourceName: "FotoPerfil"))
            self.name.text = user.name!
            self.phone.text = user.phone!
            self.email.text = user.email!
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func unRegisterForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWasShown(_ aNotification: NSNotification) {
        var info = aNotification.userInfo!
        let kbSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        var aRect = self.view.frame
        aRect.size.height -= kbSize.height
        guard let finalActiveField = self.activeField else { return }
        if !aRect.contains(finalActiveField.frame.origin) {
            self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
        }
    }
    
    func keyboardWillBeHidden(_ aNotification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeField = nil
    }
}

//
//  OrderDetailViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit

class OrderDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var frecuency: UILabel!
    @IBOutlet weak var payments: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var observations: UILabel!
    @IBOutlet weak var orderDetailTableView: UITableView!
    
    var userOrder: UserOrder!
    let reuseIdentifier = "itemCell"
 
    override func viewDidLoad() {
        super.viewDidLoad()

        self.id.text = "Orden #: \(self.userOrder.order!.id!)"
        self.amount.text = "Total: \(self.userOrder.order!.total!)"
        self.frecuency.text = "Frecuencia de pago: \(self.userOrder.order!.paymentFrecuency ?? "")"
        self.observations.text = "Observaciones: \(self.userOrder.order!.observations ?? "")"
        self.status.text = "Status: \(self.userOrder.order!.status ?? "")"
        self.payments.text = "Número de pagos: \(self.userOrder.order!.paymentFrecuency ?? "")"
        self.orderDetailTableView.delegate = self
        self.orderDetailTableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userOrder.orderItems!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        let orderItem = userOrder.orderItems![indexPath.row]
        
        cell.textLabel?.text = "Producto: \(orderItem.product!)"
        cell.detailTextLabel?.text = "Precio: \(orderItem.unitPrice!) | Cantidad: \(orderItem.quantity!) | Total: \(orderItem.totalPrice!)"
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

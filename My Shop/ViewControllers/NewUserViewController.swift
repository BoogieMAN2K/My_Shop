//
//  NewUserViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 30/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import EZSwiftExtensions
import KRProgressHUD

class NewUserViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var name: IconTextField!
    @IBOutlet weak var phoneNumber: IconTextField!
    @IBOutlet weak var email: IconTextField!
    @IBOutlet weak var password: IconTextField!
    @IBOutlet weak var repeatPassword: IconTextField!
    @IBOutlet weak var register: IconButton!
    @IBOutlet weak var jumpToLogin: IconButton!
    
    var activeField: UITextField!
    
    @IBAction func createUser(_ sender: IconButton) {
        
        if (!self.name.validateEmpty().isValid && !self.email.validateEmpty().isValid && !self.password.validateEmpty().isValid && !self.phoneNumber.validateEmpty().isValid) {
            if (self.email.validateEmail().isValid && self.phoneNumber.validatePhoneNumber().isValid) && (self.password.text == self.repeatPassword.text) {
                
                let newUser = ["_fullname": self.name.text!, "_email": self.email.text!, "_password": self.password.text!, "_phone": self.phoneNumber.text!]

				KRProgressHUD.show()
				_ = User.create(user: newUser).observeNext(with: { (result) in
                    KRProgressHUD.dismiss()
                    if result.0 {
                        Utilities.alertMessage(viewController: self, title: "Usuario creado", message: result.1, withCompletionHandler: {
                                self.navigationController?.popViewController(animated: true)
                            })
                    } else {
                        Utilities.alertMessage(viewController: self, title: "Ha ocurrido un error", message: result.1)
                        _ = Utilities.errorMessage(title: "Ha ocurrido un error", localizeDescription: "No se ha podido crear el usuario, por favor intente nuevamente", localizeFailureReason: result.1)
                    }
                })
            }
        }
    }
    
    @IBAction func jumpToLogin(_ sender: IconButton) {
        self.dismissVC(completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.name.delegate = self
        self.phoneNumber.delegate = self
        self.email.delegate = self
        self.password.delegate = self
        self.repeatPassword.delegate = self
        
        self.registerForKeyboardNotifications()
        self.hideKeyboardWhenTappedAround()
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func unRegisterForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWasShown(_ aNotification: NSNotification) {
        var info = aNotification.userInfo!
        let kbSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height + 120, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        var aRect = self.view.frame
        aRect.size.height -= kbSize.height - 120
        guard let finalActiveField = self.activeField else { return }
        if !aRect.contains(finalActiveField.frame.origin) {
            self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
        }
    }
    
    func keyboardWillBeHidden(_ aNotification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeField = nil
    }
}

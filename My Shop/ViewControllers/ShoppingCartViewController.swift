//
//  ConfirmShoppingViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 30/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa

class ShoppingCartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var shoppingCartTableView: UITableView!
    @IBOutlet weak var confirm: IconButton!
    
    var shoppingCart = [ProductOrder]()
    let reuseIdentifier = "productCell"
  
    @IBAction func confirmShoppingCart(_ sender: IconButton) {
        self.performSegue(withIdentifier: "ConfirmationSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

		UserDefaults.standard.set(self.shoppingCart.toJSONString(prettyPrint: true), forKey: "shoppingCart")
        self.shoppingCartTableView.isScrollEnabled = true
        self.shoppingCartTableView.register(UINib(nibName: "ProductsTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
    }

    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.shoppingCart.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.toCGFloat
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ProductsTableViewCell
        let selectedProduct = shoppingCart[indexPath.row]
        
        cell.productImage.af_setImage(withURL: selectedProduct.product.image!)
        cell.name.text = selectedProduct.product.name
        cell.smallDescription.text = selectedProduct.product.description
        cell.price.text = "$\(selectedProduct.product.price * Double(selectedProduct.quantity))"
        
        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.shoppingCart.remove(at: indexPath.row)
            shoppingCartTableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: ConfirmShoppingViewController.self) {
            let destinationVC = segue.destination as! ConfirmShoppingViewController
            destinationVC.productOrder = self.shoppingCart
        }
    }

}

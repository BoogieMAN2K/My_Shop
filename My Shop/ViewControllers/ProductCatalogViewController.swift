//
//  ProductCatalogViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 30/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import KRProgressHUD

class ProductCatalogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var productTable: UITableView!

    var categoriesArray: [Category]!
    var selectedCategory: Category!
    let reuseIdentifier = "menuCell"
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCategory = categoriesArray[indexPath.row]
        self.performSegue(withIdentifier: "ProductListSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! MenuTableViewCell
        let category = categoriesArray[indexPath.row]
        
        cell.itemImage.af_setImage(withURL: category.icon)
        cell.name.text = category.name
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.categoriesArray = []
  
        self.productTable.isScrollEnabled = true
        self.productTable.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)

        KRProgressHUD.show()
        let categoriesSignal = Category.all()
        _ = categoriesSignal.observeNext {
            KRProgressHUD.dismiss()
            self.categoriesArray = $0
            self.productTable.reloadData()
        }
        
    }
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: ProductListTableViewController.self) {
            let destinationVC = segue.destination as! ProductListTableViewController
            destinationVC.categoryId = self.selectedCategory.id
        }
     }


}

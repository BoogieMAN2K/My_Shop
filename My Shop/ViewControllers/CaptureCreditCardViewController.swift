//
//  CaptureCreditCardViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/14/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import UIKit
import KRProgressHUD
import IWCocoa

class CaptureCreditCardViewController: BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource {

	@IBOutlet weak var cardholder: IconTextField!
	@IBOutlet weak var cardNumber: IconTextField!
	@IBOutlet weak var cvv2: IconTextField!
	@IBOutlet weak var month: IconTextField!
	@IBOutlet weak var year: IconTextField!
	@IBOutlet weak var processPayment: UIButton!
	@IBOutlet weak var creditCardsPickerView: UIPickerView!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var cancelButton: IconButton!


	private let openpay = Openpay()
	private var tokenID: String!
	private var creditCards = [OPCard]()
	private var selectedCard: OPCard!
	var delegate: ProcessPaymentDelegate!

	override func viewDidLoad() {
		super.viewDidLoad()

		let textfields: [UITextField] = [self.cardholder, self.cardNumber, self.cvv2, self.month, self.year]
		registerForAutoScroll(scrollView: scrollView, textFields: textfields)

		self.navigationItem.setHidesBackButton(true, animated:true);
		self.creditCardsPickerView.delegate = self
		self.creditCardsPickerView.dataSource = self

		KRProgressHUD.show()
		guard let opIdentifier = UserDefaults.standard.string(forKey: "openPayId") else { return }
		_ = OPCard.retrieveAll(client: opIdentifier).observeNext(with: { (creditCards) in
			KRProgressHUD.dismiss()
			self.creditCards = creditCards
			self.creditCardsPickerView.reloadAllComponents()
		})
	}

	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}

	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return self.creditCards.count
	}

	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		if row == 0 {
			return "Agregue una nueva tarjeta de crédito..."
		}

		var creditCardString = ""
		switch self.creditCards[row - 1].cardType {
		case .OPCardTypeAmericanExpress:
			creditCardString = "American Express"
		case .OPCardTypeMastercard:
			creditCardString = "Mastercard"
		case .OPCardTypeVisa:
			creditCardString = "Visa"
		default:
			creditCardString = "Desconocida"
		}
		return "\(creditCardString) \(self.creditCards[row - 1].cardNumber!)"
	}

	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		self.cardholder.isEnabled = (row == 0)
		self.cardNumber.isEnabled = (row == 0)
		self.cvv2.isEnabled = (row == 0)
		self.month.isEnabled = (row == 0)
		self.year.isEnabled = (row == 0)

		if row != 0 {
			self.selectedCard = self.creditCards[row - 1]
			self.cardholder.text = self.selectedCard.holderName
			self.cardNumber.text = self.selectedCard.cardNumber
			self.month.text = "XX"
			self.year.text = "XX"
			self.cvv2.text = "XXXX"
		} else {
			self.selectedCard = nil
			self.cardholder.text = String.init()
			self.cardNumber.text = String.init()
			self.month.text = String.init()
			self.year.text = String.init()
			self.cvv2.text = String.init()

		}
	}

	@IBAction func cancelPayment(_ sender: UIButton) {
		self.popVC()
	}

	@IBAction func processPaymentAction(_ sender: UIButton) {
		let card = OPCard()
		card.holderName = self.cardholder.text!
		card.cardNumber = self.cardNumber.text!
		card.cvv2 = self.cvv2.text!
		card.expirationMonth = Int(self.month.text!)
		card.expirationYear = Int(self.year.text!)

		if (self.selectedCard == nil) && !card.valid {
			if card.errors.count > 0 {
				card.errors.forEach({ (result) in
					let result = result as! String

					if result == "Card number is not valid" {
						Utilities.alertMessage(viewController: self, title: "Tarjeta inválida",
						                       message: "Número de tarjeta inválido")
					}

					if result == "Card expired is not valid" {
						Utilities.alertMessage(viewController: self, title: "Tarjeta inválida",
						                       message: "Fecha de vencimiento inválida")
					}
				})

				return
			}
		}


		if self.selectedCard == nil {
			KRProgressHUD.show()
			if UserDefaults.standard.string(forKey: "openPayId") != nil {
				_ = OPCard.create(card: card, client: UserDefaults.standard.string(forKey: "openPayId")!).doOn(next: { (creditCardResult) in
					KRProgressHUD.dismiss()
					self.delegate.processPayment(with: creditCardResult.identifier!)
				}, failed: { (error) in
					KRProgressHUD.dismiss()
					Utilities.alertMessage(viewController: self, title: "Ha ocurrido un error", message: error.errorDescription!)
				}).observe { _ in }
			} else {
				let client = OPClient()
				client.externalID = UserDefaults.standard.string(forKey: "Id")
				client.name = UserDefaults.standard.string(forKey: "name")
				client.lastname = ""
				client.email = UserDefaults.standard.string(forKey: "email")
				client.requiresAccount = false
				client.phone = UserDefaults.standard.string(forKey: "phone")

				let createClient = OPClient.create(client: client)
				_ = createClient.observeNext(with: { cliente in
					let defaults = UserDefaults.standard
					defaults.set(cliente.identifier!, forKey: "openPayId")

					_ = OPCard.create(card: card, client: cliente.identifier!).doOn(next: { (creditCardResult) in
						KRProgressHUD.dismiss()
						self.delegate.processPayment(with: creditCardResult.identifier!)
					}, failed: { (error) in
						KRProgressHUD.dismiss()
						Utilities.alertMessage(viewController: self, title: "Ha ocurrido un error", message: error.errorDescription!)
					}).observe { _ in }
				})
			}
		} else {
			self.delegate.processPayment(with: self.selectedCard.identifier!)
		}
	}
}

//
//  SalesTableViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import KRProgressHUD

class SalesTableViewController: UITableViewController {
    
    var offers = [Offer]()
    var selectedOffer: Offer!
    let reuseIdentifier = "offerCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let offersSignal = Offer.all()
        KRProgressHUD.show()
        _ = offersSignal.observeNext(with: { (result) in
            KRProgressHUD.dismiss()
            self.offers = result
            self.tableView.reloadData()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        let offer = offers[indexPath.row]
        
        cell.textLabel?.text = offer.description!
        cell.detailTextLabel?.text = "Fecha Inicio:\(offer.start.toString(format: "dd/MM/yyyy")) | Fecha Final:\(offer.start.toString(format: "dd/MM/yyyy"))"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedOffer = self.offers[indexPath.row]
        self.performSegue(withIdentifier: "SalesDetailSegue", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: SalesDetailViewController.self) {
            let destinationVC = segue.destination as! SalesDetailViewController
            destinationVC.selectedOffer = self.selectedOffer
        }
    }

}

//
//  CommentsTableViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa

class CommentsViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var comments: UITextField!
    @IBOutlet weak var sendComments: IconButton!

    var activeField: UITextField!
    
    @IBAction func confirmSendComments(_ sender: IconButton) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.name.delegate = self
        self.email.delegate = self
        self.phone.delegate = self
        self.comments.delegate = self
        
        self.registerForKeyboardNotifications()
        self.hideKeyboardWhenTappedAround()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func unRegisterForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWasShown(_ aNotification: NSNotification) {
        var info = aNotification.userInfo!
        let kbSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        var aRect = self.view.frame
        aRect.size.height -= kbSize.height
        guard let finalActiveField = self.activeField else { return }
        if !aRect.contains(finalActiveField.frame.origin) {
            self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
        }
    }
    
    func keyboardWillBeHidden(_ aNotification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeField = nil
    }
}

//
//  MainViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import ImageSlideshow
import EZSwiftExtensions
import KRProgressHUD
import UserNotifications
import IWCocoa

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var slideShow: ImageSlideshow!
    @IBOutlet weak var categoryList: UIButton!
    @IBOutlet weak var categoriesTableView: UITableView!
    var imageArray = [UIImage]()
    var categoriesArray: [Category]!
    var selectedCategory: Category!
    let reuseIdentifier = "menuCell"
	var openpayObject = Openpay()

    @IBAction func categoryList(_ sender: UIButton) {
        self.performSegue(withIdentifier: "CategorySegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBarColor = UIColor.init(r: 255, g: 95, b: 96)
        self.navigationController!.navigationBar.tintColor = UIColor.white
        
        imageArray = [#imageLiteral(resourceName: "Slider"), #imageLiteral(resourceName: "Slider2"), #imageLiteral(resourceName: "Slider3")]
        var imagesources = [ImageSource]()
        for image in imageArray {
            imagesources.append(ImageSource(image: image))
        }
        self.slideShow.setImageInputs(imagesources)
        self.slideShow.contentScaleMode = .scaleAspectFit
        self.slideShow.slideshowInterval = 5
        
        self.categoriesArray = []
        self.categoriesTableView.isScrollEnabled = true
        self.categoriesTableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        
        KRProgressHUD.show()
        let categoriesSignal = Category.all()
        _ = categoriesSignal.observeNext {
            KRProgressHUD.dismiss()
            self.categoriesArray = $0
            self.categoriesTableView.reloadData()
        }
        
        let notificationSignal = ServerNotification.all()
        KRProgressHUD.show()
        _ = notificationSignal.observeNext(with: { (notifications) in
            
            notifications.forEachEnumerated({ (index, notification) in
                var dateInfo = DateComponents()
                dateInfo.second = 5
                
                if #available(iOS 10.0, *) {
                    let content = UNMutableNotificationContent()
                    content.title = NSString.localizedUserNotificationString(forKey: "Hay actualizaciones en tus compras", arguments: nil)
                    content.body = NSString.localizedUserNotificationString(forKey: "La compra \(notification.id_object!) tiene un nuevo estado de: \(notification.description!)",
                        arguments: nil)
                    
                    // Create the request object.
                    let trigger = UNCalendarNotificationTrigger(dateMatching: dateInfo, repeats: false)
                    let request = UNNotificationRequest(identifier: "AlertaCompra", content: content, trigger: trigger)
                    let center = UNUserNotificationCenter.current()
                    center.add(request) { (error : Error?) in
                        if let theError = error {
                            print(theError.localizedDescription)
                        }
                    }
                } else {
                    let notification:UILocalNotification = UILocalNotification()
                    notification.alertBody = "Hay actualizaciones en tus compras"
                    notification.fireDate = Date.init()
                    
                    UIApplication.shared.scheduleLocalNotification(notification)
                }
            })
        })

		KRProgressHUD.show()
		self.openpayObject.createDeviceSessionId(successFunction: { (sessionId) in
			KRProgressHUD.dismiss()
			UserDefaults.standard.set(sessionId, forKey: "sessionId")
		}, failureFunction: { (error) in
			KRProgressHUD.dismiss()
			Utilities.alertMessage(viewController: self, title: "Error inesperado",
			                       message: "Ha ocurrido un error inesperado, por favor salga de la aplicación y vuelva a entrar")
		})


    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCategory = self.categoriesArray[indexPath.row]
        self.performSegue(withIdentifier: "ProductListSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! MenuTableViewCell
        let category = categoriesArray[indexPath.row]
        
        cell.itemImage.af_setImage(withURL: category.icon)
        cell.name.text = category.name
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: ProductListTableViewController.self) {
            let destinationVC = segue.destination as! ProductListTableViewController
            destinationVC.categoryId = self.selectedCategory.id
        }
    }
    
}

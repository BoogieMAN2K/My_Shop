//
//  ShopProductViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 30/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import ReactiveKit

class ShopProductViewController: UIViewController {

	@IBOutlet weak var quantity: UILabel!
	@IBOutlet weak var productStep: UIStepper!
	@IBOutlet weak var confirm: IconButton!
	@IBOutlet weak var cancel: IconButton!
	@IBOutlet weak var modalView: UIView!

	var selectedProduct: Product!
	var delegate: ShoppingCartDelegate!

	typealias productOrderType = (product:Product, quantity:Int)

	@IBAction func cancel(_ sender: IconButton) {
		self.dismissVC(completion: nil)
	}

	@IBAction func confirm(_ sender: IconButton) {
		self.dismissVC {
			var productOrder = ProductOrder()
			productOrder.product = self.selectedProduct
			productOrder.quantity = Int(self.quantity.text!)!
			self.delegate.addToShoppingCart(productOrder: productOrder)
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		self.modalView.layer.cornerRadius = 30
		self.modalView.layer.borderWidth = 2
		self.modalView.layer.borderColor = UIColor.red.cgColor
		self.view.backgroundColor = UIColor.clear

		_ = quantity.bind(signal: productStep.reactive.value.map{ $0.toInt.toString }.toSignal())
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

}

//
//  OrdersTableViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import KRProgressHUD

class OrdersTableViewController: UITableViewController {

    var orders = [UserOrder]()
    let reuseIdentifier = "orderCell"
    var selectedOrder: UserOrder!

    override func viewDidLoad() {
        super.viewDidLoad()

        let orderSignal = UserOrder.all()
        KRProgressHUD.show()
        
        _ = orderSignal.observeNext(with: { (orders) in
            KRProgressHUD.dismiss()
            self.orders = orders
            self.tableView.reloadData()
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orders.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        let order = orders[indexPath.row]
        
        cell.textLabel?.text = "Orden #: \(order.order!.id!)"
        cell.detailTextLabel?.text = "Status: \(order.order!.status!)"

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedOrder = orders[indexPath.row]
        self.performSegue(withIdentifier: "OrderDetailSegue", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: OrderDetailViewController.self) {
            let destinationVC = segue.destination as! OrderDetailViewController
            destinationVC.userOrder = self.selectedOrder
        }
    }
}

//
//  MenuTableViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import KRProgressHUD
import IWCocoa
import ObjectMapper

class MenuTableViewController: UITableViewController, ProcessPaymentDelegate {
    
    @IBOutlet weak var profilePicture: ProfilePictures!
    @IBOutlet weak var userName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.value(forKey: "image") != nil {
            let userDefault = UserDefaults.standard
            self.profilePicture.af_setImage(withURL: userDefault.url(forKey: "image")!, placeholderImage: #imageLiteral(resourceName: "FotoPerfil"))
            self.userName.text = userDefault.string(forKey: "name")
        } else {
            let userInfoSignal = User.info()
            KRProgressHUD.show()
            _ = userInfoSignal.observeNext(with: { (user) in
                KRProgressHUD.dismiss()
                guard let image = user.image else { return }
                self.profilePicture.af_setImage(withURL: image)
                self.userName.text = user.name!
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 7:
            self.view.window?.rootViewController = UIStoryboard.init(name: "Startup", bundle: Bundle.main).instantiateInitialViewController()
            UserDefaults.resetStandardUserDefaults()
            UserDefaults.standard.synchronize()
            UserDefaults.standard.set(false, forKey: "hasLoginKey")
        default:
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.destination.isKind(of: ShoppingCartViewController.self) {
			let destinationVC = segue.destination as! ShoppingCartViewController
			guard let shoppingString = UserDefaults.standard.string(forKey: "shoppingCart") else { return }
			destinationVC.shoppingCart = Mapper<ProductOrder>().mapArray(JSONString: shoppingString)!
		}

		if segue.destination.isKind(of:ProcessPaymentViewController.self) {
			let destinationVC = segue.destination as! ProcessPaymentViewController
			guard let shoppingString = UserDefaults.standard.string(forKey: "shoppingCart") else { return }
			let productsOrder = Mapper<ProductOrder>().mapArray(JSONString: shoppingString)!
			let paymentMethodSignal = PaymentMethod.all()
			var charge = Double(0)
			var shoppingCart = ShoppingCart()
			KRProgressHUD.show()
			_ = paymentMethodSignal.observeNext(with: { (result) in
				guard let monthPayment = result.first(where: { $0.name == "OpenPay" }) else { return }

				var products = Array<ShoppingCart.ShoppingProduct>()
				productsOrder.forEachEnumerated {
					var product = ShoppingCart.ShoppingProduct()
					product.cant = $1.quantity
					product.product = $1.product.id!.toString
					charge = Double(product.cant) * $1.product.cost!
					products.append(product)
				}
				shoppingCart.address = UserDefaults.standard.string(forKey: "address")!
				shoppingCart.cantPays = "1"
				shoppingCart.frecuency = "1"
				shoppingCart.name = UserDefaults.standard.string(forKey: "name")!
				shoppingCart.paymentMethod = monthPayment.id!.toString
				shoppingCart.phone = UserDefaults.standard.string(forKey: "phone")!
				shoppingCart.products = products
				destinationVC.shoppingCart = shoppingCart
				destinationVC.creditCardId = UserDefaults.standard.string(forKey: "creditCardId")!
				destinationVC.sessionID = UserDefaults.standard.string(forKey: "sessionId")
				destinationVC.orderDetail = productsOrder
				destinationVC.amountToPay = Double(charge)

				KRProgressHUD.dismiss()
			})
		}
    }

	func processPayment(with CreditCard: String) {
		UserDefaults.standard.set(CreditCard, forKey: "creditCardId")
		self.performSegue(withIdentifier: "ProcessPayment", sender: self)
	}

}

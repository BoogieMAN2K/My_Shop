//
//  ConfirmShoppingViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import KRProgressHUD

class ConfirmShoppingViewController: BaseViewController, ProcessPaymentDelegate {

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var name: IconTextField!
	@IBOutlet weak var phone: IconTextField!
	@IBOutlet weak var address: IconTextField!
	@IBOutlet weak var charge: UILabel!
	@IBOutlet weak var confirmShop: IconButton!

	var productOrder = [ProductOrder]()
	var paymentMethod: PaymentMethod!
	var creditCardId: String!
	var sessionID: String!
	var shoppingCart = ShoppingCart()
	var openpayObject = Openpay()

	@IBAction func confirmProcess(_ sender: IconButton) {
		let paymentMethodSignal = PaymentMethod.all()
		KRProgressHUD.show()
		_ = paymentMethodSignal.observeNext(with: { (result) in
			guard let monthPayment = result.first(where: { $0.name == "OpenPay" }) else { return }

			var products = Array<ShoppingCart.ShoppingProduct>()
			self.productOrder.forEachEnumerated {
				var product = ShoppingCart.ShoppingProduct()
				product.cant = $1.quantity
				product.product = $1.product.id!.toString
				products.append(product)
			}

			self.shoppingCart.address = self.address.text!
			self.shoppingCart.cantPays = "1"
			self.shoppingCart.frecuency = "1"
			self.shoppingCart.name = self.name.text!
			self.shoppingCart.paymentMethod = monthPayment.id!.toString
			self.shoppingCart.phone = self.phone.text!
			self.shoppingCart.products = products

			self.performSegue(withIdentifier: "CapturePaymentInfo", sender: self)
		})
	}

	func processPayment(with CreditCard: String) {
		self.creditCardId = CreditCard
		UserDefaults.standard.set(self.creditCardId, forKey: "creditCardId")
		self.performSegue(withIdentifier: "ProcessPayment", sender: self)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		self.navigationItem.leftItemsSupplementBackButton = true;

		self.address.text = UserDefaults.standard.string(forKey: "address")
		self.phone.text = UserDefaults.standard.string(forKey: "phone")
		self.name.text = UserDefaults.standard.string(forKey: "name")

		var totalPayment: Double = 0
		for payment in self.productOrder {
			totalPayment = totalPayment + (payment.product.price * Double(payment.quantity))
		}
		self.charge.text = totalPayment.toString

		let textfields: [UITextField] = [self.name, self.phone, self.address]
		self.registerForAutoScroll(scrollView: scrollView, textFields: textfields)

		self.sessionID = String.init()
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.destination.isKind(of:ProcessPaymentViewController.self) {
			let destinationVC = segue.destination as! ProcessPaymentViewController
			destinationVC.shoppingCart = self.shoppingCart
			destinationVC.creditCardId = self.creditCardId
			destinationVC.sessionID = UserDefaults.standard.string(forKey: "sessionId")
			destinationVC.orderDetail = self.productOrder
			destinationVC.amountToPay = Double(self.charge.text!)
		}

		if segue.destination.isKind(of:CaptureCreditCardViewController.self) {
			let destinationVC = segue.destination as! CaptureCreditCardViewController
			destinationVC.delegate = self
		}
		
	}
}

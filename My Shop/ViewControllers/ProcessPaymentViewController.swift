//
//  ProcessPaymentViewController.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/17/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import UIKit
import KRProgressHUD
import IWCocoa

final class ProcessPaymentViewController: UIViewController {

	@IBOutlet weak var cardNumber: UILabel!
	@IBOutlet weak var cardHolder: UILabel!
	@IBOutlet weak var amount: UILabel!

	var amountToPay: Double!
	var orderDetail: [ProductOrder]!
	var creditCardId: String!
	var sessionID: String!
	var shoppingCart: ShoppingCart!
	private var creditCard = OPCard()

	@IBAction func processPaymentAction(_ sender: UIButton) {
		KRProgressHUD.show()
		var products = String.init()
		self.orderDetail.forEachEnumerated {
			products += "\($1.product.name!) x \($0) \n"
		}

		_ = OPPayment.charge(amount: self.amountToPay!,
		                     description: "Compra \(products)",
			order: nil, session: self.sessionID).doOn(next: { (response) in
				KRProgressHUD.dismiss({
					let alert = UIAlertController.init(title: "Pago Completado", message: "Se ha hecho el cargo correctamente",
					                                   preferredStyle: .alert)
					let okAction = UIAlertAction.init(title: "Ok", style: .default, handler:  { (alertAction) in
						self.shoppingCart.openPayId = UserDefaults.standard.string(forKey: "openPayId")!
						self.shoppingCart.openPayOrder = response.id!
						var tempCreditCard = ShoppingCart.Card()
						tempCreditCard.alias = response.card?.card_number?.substring(from: (response.card?.card_number?.length)! - 4)
						tempCreditCard.openPayId = self.creditCardId!
						tempCreditCard.type = response.card?.brand!
						self.shoppingCart.creditCard = tempCreditCard
						_ = ShoppingCart.create(order: self.shoppingCart).observeNext { (result) in
							UserDefaults.standard.removeObject(forKey: "shoppingCart")
							UserDefaults.standard.synchronize()
							if result.0 {
								Utilities.alertMessage(viewController: self, title: "Orden creada", message: result.1)
							} else {
								Utilities.alertMessage(viewController: self, title: "Error enviando pedido", message: result.1)
								_ = Utilities.errorMessage(title: "Error enviando pedido",
								                           localizeDescription: "Ha ocurrido un error solicitando la compra, intente nuevamente",
								                           localizeFailureReason: "Posible error comunicando con el servidor")
							}
						}
						self.popToRootVC()
					})
					alert.addAction(okAction)
					self.presentVC(alert)
				})
			}, failed: {(error) in
				KRProgressHUD.dismiss({
					let alert = UIAlertController.init(title: "Error procesando pago",
					                                   message: "Ha ocurrido un error mientras se procesaba el pago, por favor intente de nuevo",
					                                   preferredStyle: .alert)
					let cancelAction = UIAlertAction.init(title: "Cancelar", style: .cancel, handler: { (alertAction) in
						self.popVC()
					})
					let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
					alert.addAction(okAction)
					alert.addAction(cancelAction)
					self.presentVC(alert)
				})
			}).observe { _ in }
	}

	@IBAction func cancelPaymentAction(_ sender: UIButton) {
		self.popVC()
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		let userData = UserDefaults.standard
		KRProgressHUD.show()
		_ = OPCard.retrieve(identifier: self.creditCardId, client: userData.string(forKey: "openPayId")!).doOn(next: { (card) in
			self.creditCard = card
			self.cardNumber.text = "Número de tarjeta: \(card.cardNumber!)"
			self.cardHolder.text = "Tarjetahabiente: \(card.holderName!)"
			self.amount.text = "Monto a cancelar: \(self.amountToPay!)"
			KRProgressHUD.dismiss()
		}).observe { _ in }
	}
}

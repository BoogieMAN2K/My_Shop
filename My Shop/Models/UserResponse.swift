//
//  UserResponse.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper

class UserResponse: Mappable {

    var user: [User]!
    
    init() {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        user <- map["user"]
    }
    
}

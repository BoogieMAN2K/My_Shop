//
//  Frecuency.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct Frecuency: Mappable {
    
    var id: Int!
    var name: String!
    var daysNumber: Int8!
    var minimunPays: Int16!
    var maximunPays: Int16!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["description"]
        daysNumber <- map["daysNumber"]
        minimunPays <- map["minPays"]
        maximunPays <- map["maxPays"]
    }
    
    static func all() -> Signal<[Frecuency], NSError> {
        let frecuencySignal = Services.requestArray(url: "payment-frequency/all", method: .get, returnType: [Frecuency()])
        
        return frecuencySignal.map { $0.1 }
    }

}

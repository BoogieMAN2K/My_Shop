//
//  Store.swift
//  My Shop
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct Store: Mappable {
    
    var id: Int!
    var name: String!
    var address: String!
    var mainPhone: String!
    var secondPhone: String!
    var email: String!
    var latitude: Double!
    var longitude: Double!
    var image: URL!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["storeName"]
        address <- map["address"]
        mainPhone <- map["phone1"]
        secondPhone <- map["phone2"]
        email <- map["email"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        image <- (map["image"], URLTransform())
    }
    
    static func all() -> Signal<[Store], NSError> {
        let storeSignal = Services.requestArray(url: "store/all", method: .get, returnType: [Store()])
        
        return storeSignal.map { $0.1 }
    }

}

//
//  ProductOrder.swift
//  My Shop
//
//  Created by Victor Alejandria on 3/7/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper

struct ProductOrder: Mappable {

	var product: Product!
	var quantity: Int!

	init() {
	}

	init?(map: Map) {
		//Required
	}

	mutating func mapping(map: Map) {
		product <- map["product"]
		quantity <- map["quantity"]
	}


}

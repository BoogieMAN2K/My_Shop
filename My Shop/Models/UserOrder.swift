//
//  UserOrder.swift
//  My Shop
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct UserOrder: Mappable {
    
    var order : Order?
    var orderItems : Array<OrderItem>?

    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        order <- map["order"]
        orderItems <- map["orderItems"]
    }
    
    static func all() -> Signal<[UserOrder], NSError> {
        let orderSignal = Services.requestArray(url: "order/get", method: .get, returnType: [UserOrder()])
        
        return orderSignal.map { $0.1 }
    }
    
}

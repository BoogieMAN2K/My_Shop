//
//  PaymentMethod.swift
//  My Shop
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct PaymentMethod: Mappable {
    
    var id: Int!
    var name: String!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["description"]
    }
    
    static func all() -> Signal<[PaymentMethod], NSError> {
        let paymentMethodSignal = Services.requestArray(url: "payment-method/all", method: .get, returnType: [PaymentMethod()])
        
        return paymentMethodSignal.map { $0.1 }
    }
    
}

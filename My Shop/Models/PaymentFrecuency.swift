//
//  PaymentFrecuency.swift
//  My Shop
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct PaymentFrecuency: Mappable {
    
    var id: Int!
    var name: String!
    var daysNumber: Int8!
    var minPays: Int8!
    var maxPays: Int8!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["description"]
        daysNumber <- map["daysNumber"]
        minPays <- map["minPays"]
        maxPays <- map["maxPays"]
    }
    
    static func all() -> Signal<[PaymentFrecuency], NSError> {
        let paymentFrecuencySignal = Services.requestArray(url: "payment-frecuency/all", method: .get, returnType: [PaymentFrecuency()])
        
        return paymentFrecuencySignal.map { $0.1 }
    }
    
}

//
//  Response.swift
//  Gaver
//
//  Created by Victor Alejandria on 24/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper

struct Response<T>: Mappable {
    
    var status: Int8!
    var response: T?
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        status <- map["status"]
        response <- map["result"]
    }
}

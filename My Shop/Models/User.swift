//
//  User.swift
//  My Shop
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct User: Mappable {
    
    var userToken: String!
    var id: Int!
    var name: String!
    var email: String!
    var phone: String!
    var password: String!
    var lastLogin: Date!
    var created: Date!
    var image: URL!
    var status: UserStatus!
	var openPayId: String!
    
    enum UserStatus: String {
        case activo = "Activo"
        case inactivo = "Inactivo"
        case bloqueado = "Bloqueado"
    }
    
    init() {
        let userDefaults = UserDefaults.standard
        if userDefaults.value(forKey: "token") == nil { return }
        self.id = userDefaults.integer(forKey: "id")
        self.userToken = userDefaults.string(forKey: "token")
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        userToken <- map["token"]
        id <- map["id"]
        name <- map["fullname"]
        email <- map["email"]
        phone <- map["phone"]
        password <- map["password"]
        image <- (map["image"], URLTransform())
        lastLogin <- (map["lastLogin"], DateTransform())
        created <- (map["createdAt"], DateTransform())
        status <- (map["enabled"], EnumTransform<UserStatus>())
		openPayId <- map["openpayUserId"]
    }
    
    static func create(user: [String: Any]) -> Signal1<(Bool, String)> {
        let userSignal = Services.request(url: "register", parameters: user, method: .post, headers: ["Content-Type":"application/json"], returnType: FullResponse<String>())
        
        let create = Signal1<(Bool, String)> { observer in
            _ = userSignal.observeNext { (result) in
                if (result.1.message! == "Estas a un paso de ser usuario MyShop, hemos enviado a tu correo un link para validar tu registro. Revisa tu bandeja de entrada y unete a la familia MyShop.") {
                    observer.next((true, result.1.message!))
                } else {
                    observer.next((false, result.1.message!))
                }
                observer.completed()
            }
            
            return NonDisposable.instance
        }
        
        return create
    }
    
    static func update(user: User) -> Signal<Bool, NSError> {
        let parameters = ["_fullname":user.name, "_email":user.email, "_phone":user.phone] as [String:Any]
        let userResponse = Services.request(url: "account/update-information", parameters: parameters, method: .post, returnType: User())
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(user.name, forKey: "name")
        userDefaults.set(user.phone, forKey: "phone")
        userDefaults.set(user.email, forKey: "email")
        
        return userResponse.map { $0.0 }
    }
    
    static func updatePassword(password: String) -> Signal<Bool, NSError> {
        let parameters = ["password":password] as [String:Any]
        let userResponse = Services.request(url: "account/update-password", parameters: parameters, method: .post,
                                            returnType: User())
        
        return userResponse.map { $0.0 }
    }
    
    static func updateProfilePicture(picture: UIImage) -> Signal<Bool, NSError> {
        let temp = picture.toBase64(size: CGSize(width: 800, height: 800))
        let parameters = ["image" : "data: image/jpeg;base64,\(temp)"]
        let userResponse = Services.request(url: "account/update-image", parameters: parameters, method: .post,
                                            returnType: FullResponse<String>())
        
        return userResponse.map { $0.0 }
    }
    
    static func recoverPassword(email: String) -> Signal<Bool, NSError> {
        let parameters = ["_email":email] as [String:Any]
        let userResponse = Services.request(url: "account/reset-password", parameters: parameters, method: .post,
                                            headers: ["Content-Type":"application/json"], returnType: FullResponse<String>())
        
        return userResponse.map { $0.0 }
    }
    
    static func login(username: String, password: String) -> Signal1<Bool> {
        let loginCheck = Security.checkLogin(username: username, password: password)
        let login = Signal1<Bool> { observer in
            _ = loginCheck.observeNext { (result) in
                if result {
                    observer.next(true)
                } else {
                    observer.next(false)
                }
                observer.completed()
            }
            
            return NonDisposable.instance
        }
        
        return login
    }
    
    static func info() -> Signal<User, NSError> {
        let userResponse = Services.request(url: "account/all", method: .get, returnType: UserResponse())
        
        return userResponse.map {
            let user = $0.1.user[0]
            
            let userDefaults = UserDefaults.standard
            userDefaults.set(user.name, forKey: "name")
            userDefaults.set(user.phone, forKey: "phone")
            userDefaults.set(user.email, forKey: "email")
            userDefaults.set(user.id, forKey: "id")
            userDefaults.set(user.image, forKey: "image")
            userDefaults.set(user.lastLogin, forKey: "lastLogin")
            userDefaults.set(user.status.rawValue, forKey: "status")
			userDefaults.set(user.openPayId, forKey: "openPayId")
            userDefaults.synchronize()
            
            return user
        }
    }
    
}

//
//  Offer.swift
//  My Shop
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct Offer: Mappable {
    
    var id: Int!
    var description: String!
    var start: Date!
    var finish: Date!
    var image: URL!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        description <- map["description"]
        start <- (map["start"], DateTransform())
        finish <- (map["finish"], DateTransform())
        image <- (map["image"], URLTransform())
    }
    
    static func all() -> Signal<[Offer], NSError> {
        let offerSignal = Services.requestArray(url: "offer/all", method: .get, returnType: [Offer()])
        
        return offerSignal.map { $0.1 }
    }
    
}

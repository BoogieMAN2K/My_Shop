//
//  Product.swift
//  My Shop
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct Product: Mappable {
    
    var id: Int!
    var name: String!
    var description: String!
    var image: URL!
    var price: Double!
    var cost: Double!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        image <- (map["image"], URLTransform())
        price <- map["price"]
        cost <- map["cost"]
    }
    
    static func all() -> Signal<[Product], NSError> {
        let productSignal = Services.requestArray(url: "product/all", method: .get, returnType: [Product()])
        
        return productSignal.map { $0.1 }
    }
    
    static func forCategory(id: Int) -> Signal<[Product], NSError> {
        let productSignal = Services.requestArray(url: "product/for-category/\(id)", method: .get, returnType: [Product()])
        
        return productSignal.map { $0.1 }
    }
    
    static func show(id: Int) -> Signal<Product, NSError> {
        let productSignal = Services.requestArray(url: "product/detail/\(id)", method: .get, returnType: [Product()])
        
        return productSignal.map { $0.1[0] }
    }
    
}

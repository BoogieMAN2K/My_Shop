//
//  GenericResponse.swift
//  My Shop
//
//  Created by Victor Alejandria on 10/24/16.
//  Copyright © 2016 ISMCenter. All rights reserved.
//
import ObjectMapper

struct FullResponse<T>: Mappable {
    
    var status: Int16!
    var response: T?
    var message: String!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        status <- map["code"]
        message <- map["message"]
        response <- map["result"]
    }
    
}

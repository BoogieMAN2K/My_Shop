//
//  ShoppingCart.swift
//  My Shop
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct ShoppingCart: Mappable {
    
    var frecuency : String!
    var cantPays : String!
    var paymentMethod : String!
    var name: String!
    var address: String!
    var phone: String!
    var products: [ShoppingProduct]!
	var openPayId: String!
	var openPayOrder: String!
	var creditCard: Card!
    
    struct ShoppingProduct: Mappable {
        var cant: Int!
        var product: String!
        
        init() {
        }
        
        init?(map: Map) {
            //Required
        }
        
        mutating func mapping(map: Map) {
            cant <- map["cant"]
            product <- map["product"]
        }
    }
    
	struct Card: Mappable {
		var type: String!
		var alias: String!
		var openPayId: String!

		init() {
		}

		init?(map: Map) {
			//Required
		}

		mutating func mapping(map: Map) {
			type <- map["type"]
			alias <- map["alies"]
			openPayId <- map["openpay_id"]
		}
	}

    init() {
    }

    init?(map: Map) {
        //Required
    }

    mutating func mapping(map: Map) {
        frecuency <- map["frecuency"]
        cantPays <- map["cant_pays"]
        paymentMethod <- map["payment_method_id"]
        name <- map["fullname"]
        phone <- map["phone"]
        address <- map["address"]
        products <- map["product"]
		openPayOrder <- map["openPayOrder"]
		creditCard <- map["card"]
		openPayId <- map["openPayId"]
    }
    
    static func create(order: ShoppingCart) -> Signal<(Bool, String), NSError> {
        let shoppingSignal = Services.request(url: "order/create", parameters: order.toJSON(), method: .post, returnType: FullResponse<String>())
        
        return shoppingSignal.map {
            if $0.1.status == 200 {
                return (true, $0.1.message)
            } else {
                return (false, $0.1.message)
            }
        }
    }
    
}

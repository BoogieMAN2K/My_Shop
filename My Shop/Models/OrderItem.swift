//
//  OrderItem.swift
//  My Shop
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct OrderItem: Mappable {
    
    var id : Int!
    var line : Int!
    var quantity : Int!
    var unitPrice : Int!
    var unitCost : Int!
    var totalPrice : Int!
    var totalCost : Int!
    var product : String!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        line <- map["line"]
        quantity <- map["cant"]
        unitPrice <- map["unitPrice"]
        unitCost <- map["unitCost"]
        totalPrice <- map["totalPrice"]
        totalCost <- map["totalCost"]
        product <- map["product"]
    }
    
    static func showFor(order: Int) -> Signal<[OrderItem], NSError> {
        let orderItemSignal = Services.requestArray(url: "order/detail/\(order)", method: .get, returnType: [OrderItem()])
        
        return orderItemSignal.map { $0.1 }
    }
    
}

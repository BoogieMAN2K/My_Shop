//
//  Order.swift
//  My Shop
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct Order: Mappable {
    
    var id : Int!
    var status : String!
    var observations : String!
    var quantityPays : Int!
    var taxable : Int!
    var taxes : Int!
    var total : Int!
    var paymentFrecuency : String!
    var description : String!

    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        status <- map["status"]
        observations <- map["obs"]
        quantityPays <- map["cantPays"]
        taxable <- map["taxable"]
        taxes <- map["taxes"]
        total <- map["total"]
        paymentFrecuency <- map["paymentFrequency"]
        description <- map["description"]
    }
    
    static func all() -> Signal<[Order], NSError> {
        let orderSignal = Services.requestArray(url: "order/all", method: .get, returnType: [Order()])
        
        return orderSignal.map { $0.1 }
    }
    
    static func show(id: Int) -> Signal<Order, NSError> {
        let orderSignal = Services.request(url: "order/detail/\(id)", method: .get, returnType: Order())
        
        return orderSignal.map { $0.1 }
    }
    
}

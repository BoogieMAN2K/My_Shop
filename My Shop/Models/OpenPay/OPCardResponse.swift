//
//  OPCardResponse.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/15/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import ObjectMapper

class OPCardResponse: Mappable {
	public var id : String?
	public var type : String?
	public var brand : String?
	public var address : String?
	public var card_number : String?
	public var holder_name : String?
	public var expiration_year : Int?
	public var expiration_month : Int?
	public var allows_charges : String?
	public var allows_payouts : String?
	public var creation_date : String?
	public var bank_name : String?
	public var bank_code : Int?

    required init?(map: Map) {
        //Required
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        brand <- map["brand"]
        address <- map["address"]
        card_number <- map["card_number"]
        holder_name <- map["holder_name"]
        expiration_year <- map["expiration_year"]
        expiration_month <- map["expiration_month"]
        allows_charges <- map["allows_charges"]
        allows_payouts <- map["allows_payouts"]
        creation_date <- map["creation_date"]
        bank_name <- map["bank_name"]
        bank_code <- map["bank_code"]
    }
    
}

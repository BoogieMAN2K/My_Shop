//
//  OPPaymentResponse.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/15/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import ObjectMapper

class OPPaymentResponse: Mappable {
	public var id : String?
	public var amount : Int?
	public var authorization : Int?
	public var method : String?
	public var operation_type : String?
	public var transaction_type : String?
	public var card : OPCardResponse?
	public var status : String?
	public var currency : String?
	public var exchange_rate : OPExchangeRateResponse?
	public var creation_date : String?
	public var operation_date : String?
	public var description : String?
	public var error_message : String?
	public var order_id : String?

    init() {
    }
    
    required init?(map: Map) {
        //Required
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        amount <- map["amount"]
        authorization <- map["authorization"]
        method <- map["method"]
        operation_type <- map["operation_type"]
        transaction_type <- map["transaction_type"]
        card <- map["card"]
        status <- map["status"]
        currency <- map["currency"]
        exchange_rate <- map["exchange_rate"]
        creation_date <- map["creation_date"]
        operation_date <- map["operation_date"]
        description <- map["description"]
        error_message <- map["error_message"]
        order_id <- map["order_id"]
    }
    
}

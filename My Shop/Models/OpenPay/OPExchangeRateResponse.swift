//
//  OPExchangeRateResponse.swift
//  My Shop
//
//  Created by Victor Alejandria on 1/15/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import ObjectMapper

class OPExchangeRateResponse: Mappable {
	public var from : String?
	public var date : String?
	public var value : Double?
	public var to : String?

    required init?(map: Map) {
        //Required
    }
    
    func mapping(map: Map) {
        from <- map["from"]
        date <- map["date"]
        value <- map["value"]
        to <- map["to"]
    }
    
}

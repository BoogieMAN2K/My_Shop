//
//  OPPayments.swift
//  My Shop
//
//  Created by Victor Alejandria on 2/13/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import Alamofire
import SwiftyJSON

class OPPayment: Mappable {
    
    var method: String?
    var source: String?
    var amount: Double?
    var currency: String?
    var description: String?
    var order: String?
    var deviceSession: String?
    var capture = true
    var customer: OPClient?
    
    private init(amount: Double, currency: String, description: String, order: String? = nil, method: String = "card", capture: Bool = true) {
        self.amount = amount
        self.capture = capture
        self.currency = currency
        self.description = description
        self.method = method
        self.order = order
    }
    
    required init?(map: Map) {
        //Required
    }
    
    func mapping(map: Map) {
        method <- map["method"]
        source <- map["source_id"]
        amount <- map["amount"]
        currency <- map["currency"]
        description <- map["description"]
        order <- map["order_id"]
        deviceSession <- map["device_session_id"]
        capture <- map["capture"]
        customer <- map["customer"]
    }
    
    static func charge(amount: Double, currency: String = "USD", description: String, order: String?, session: String) -> Signal<OPPaymentResponse, OPError>{
        let payment = OPPayment(amount: amount, currency: currency, description: description)
        let defaults = UserDefaults.standard
        let opClientID = defaults.string(forKey: "openPayId")
        let opCardID = defaults.string(forKey: "creditCardId")
        let url = "customers/\(opClientID!)/charges"
        payment.source = opCardID
        payment.deviceSession = session
        let parameters = payment.toJSON()
        
		let result = Openpay.post(url: url, parameters: parameters, encoding: JSONEncoding.default,
		                          resultType: OPPaymentResponse.self) { (response, observer) in
            if response.result.isSuccess {
                if (response.result.value?.id == nil) {
                    let json = JSON(data: response.data!)
                    let error = OPError()
                    error.category = json["category"].stringValue
                    error.errorCode = json["error_code"].intValue
                    error.errorDescription = json["error_description"].stringValue
                    error.httpCode = json["http_code"].intValue
                    observer.failed(error)
                } else {
                    observer.next(response.result.value!)
                }
            } else {
                observer.failed(response.result.error as! OPError)
            }
            
            observer.completed()
            
        }
        
        return result
    }
    
}

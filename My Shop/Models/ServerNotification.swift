//
//  Notification.swift
//  My Shop
//
//  Created by Victor Alejandria on 6/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import Alamofire
import AlamofireObjectMapper
import AlamofireImage
import ReactiveKit
import IWCocoa

struct ServerNotification: Mappable {
    
    var id: Int!
    var description: String!
    var enabled: Bool!
    var type: Int!
    var id_object: Int!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        description <- map["description"]
        enabled <- map["enabled"]
        type <- map["type"]
        id_object <- map["id_object"]
    }
    
    static func all() -> Signal<[ServerNotification], NSError> {
        let notificationSignal = Services.requestArray(url: "notification/user", method: .get, returnType: [ServerNotification()])
        
        return notificationSignal.map { $0.1 }
    }
    
}

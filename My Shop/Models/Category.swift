//
//  Category.swift
//  My Shop
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct Category: Mappable {

    var id: Int!
    var name: String!
    var icon: URL!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["description"]
        icon <- (map["icon"], URLTransform())
    }
    
    static func all() -> Signal<[Category], NSError> {
        let categorySignal = Services.requestArray(url: "category/all", method: .get, returnType: [Category()])
        
        return categorySignal.map { $0.1 }
    }
    
}

//
//  Token.swift
//  Gaver
//
//  Created by Victor Alejandria on 24/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper

struct Token: Mappable {
    
    var token: String!
    var refresh: String!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        token <- map["token"]
        refresh <- map["refresh"]
    }
}

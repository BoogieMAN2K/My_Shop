//
//  Constants.swift
//  My Shop
//
//  Created by Victor Alejandria on 25/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import Foundation

public struct Google {
    static let kGoogleMapsAPIKey = "AIzaSyBSlsn4bWBd1mxHwVkmTmlMRimCT5xI1zo"
}

public struct WebService {
    //MARK: General Server URL
    static let kServerBaseURL = "https://myshop.administradorpixel.com/api/"
    static let kProductionBaseURL = "https://myshop.administradorpixel.com/api/"
}

public struct Payment {
    
    static let kSandboxMerchantID = "mav8cuk4cgaygquogcdp"
    static let kSandboxApiKey = "sk_4748ae990c4a4fccb9b5403fdfc2b12a"
    
    static let kProductionMerchantID = ""
    static let kProductionApiKey = ""
    
}
